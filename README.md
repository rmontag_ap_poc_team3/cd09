## What is ansible-role-ansible?

It is an ansible role to install and configure:

- ansible

### Supported platforms:

- Ubuntu 16.04 LTS (Xenial) - Debian-based
- CentOS 7.3 - RedHat-based

## Roles defined

Currently this project defines the following roles:

#### role: "ansible"
- install ansible via OS-specific method

## Role variables

#### role "ansible"

Currently no Role variables are defined.

## Example playbook

```shell
$ ansible-playbook -i inventories/develop setup_ansible.yml
```

